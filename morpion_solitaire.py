"""
***********************************************                                       
Objectif  : Facade Python -> C for the  morpion solitaire code (morpion.so)
Auteur    : Inria (Julien Teigny)               
Date      : 05/05/2020

+ qq modifs de PP

***********************************************                       
"""

import gym
from ctypes import *

#Loading the shared library of the C code
morpion_so = "./morpion.so"
my_facade = CDLL(morpion_so)


class Move(Structure):
    """Transfer object for Move (t_Coup) between C and Python"""
    _fields_ = [("x", c_int), ("y", c_int),("sx", c_int), ("sy", c_int), ("dir", c_int)]
    
    
class Cell(Structure):
    """Transfer object for Cell (t_Cellule) between C and Python"""
    _fields_ = [("x", c_int), ("y", c_int),("numero", c_int), ("counts", c_int*3*3), ("connected", c_int*3*3)]
    

class Morpion_solitaire(gym.Env):
    metadata = {'render.modes': ['human']}
    numero_du_coup = 0
    nb_available_moves = -1
    
    def __init__(self):
        """
        Facade to init the game in the C code 
        """     
        my_facade.facade_init()
        my_facade.facade_reset_game()
  
    def reset(self):
        """
        Gym reset method
        Facade to reinit the board game to the initial position in the C code
        """
        my_facade.facade_reset_game()
 
    def get_observation(self):
        """
        Gym get_observation method
        Returns the observation about the current state of the game
        :return: (game_continue,board_game_information,available_moves)
        """
        #Update the board, get the number of available moves and the size of the current boardGame
        my_facade.facade_update_available_move ()
        self.nb_available_moves = my_facade.facade_get_nb_available_moves ()
        board_game_size = my_facade.facade_get_board_game_size ()
        #Get available moves : coups_possibles
        available_moves = (Move * self.nb_available_moves) ()
        my_facade. facade_get_available_moves (available_moves) 
        #Get current board game : t_jeu
        board_game_information = (Cell * board_game_size * board_game_size) ()
        my_facade.facade_get_board_game_information (board_game_information)
        #no available move ---->   game is over
        done = self.nb_available_moves == 0
        return (done, board_game_information, available_moves)

    def step(self, which_move):
        """
        Gym step method
        Play the move in the 'which_move' index from available_moves (coups_possibles) and remember it as the 'move_no' move of the game.
        :param which_move: int --> index of the available_move to play
        :return: (game_continue,board_game_information,available_moves)  the get_observation returns after the step
        """
        if (which_move < self.nb_available_moves) & (which_move >= 0):
            self.numero_du_coup += 1
            my_facade.facade_step.argtypes = [c_int, c_int]
            my_facade.facade_step (which_move, self.numero_du_coup)
        return self.get_observation()

    def render(self, mode='human'):
        """
        Display the current board game
        """        
        my_facade.facade_print_game()
        
        
        
        
