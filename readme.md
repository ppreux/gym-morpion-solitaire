Environnement gym pour jouer au morpion solitaire.

* gym_morpion_solitaire_doc.fr.pdf : une courte documentation en français
* gym_morpion_solitaire_doc.en.pdf : a short documentation in English
* morpion_solitaire.c : the source code of the simulator of the game
* morpion_solitaire.py : the gym interface to use the simulator in python
* Makefile : see the documentation
