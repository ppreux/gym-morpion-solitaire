/*
 * morpion_solitaire.c

   basé sur code morpion.c + interfaçage avec Python (- divers trucs inutiles pour cela)

   Developed by Philippe.Preux@inria.fr & Julien Teigny.

   This is LGPL software.

   http://www.morpionsolitaire.com/

 */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>

#define bit64

#define Ncoups 100 /* on suppose qu'il n'y a pas plus de Ncoups possibles */

static int Nruns = 1;
static int draw_crosses = 1;

/* d_a = anti-diagonal  /
   d_d = diagonal       \
   d_h = horizontal     -
   d_v = vertical       |
*/
typedef enum { d_a, d_d, d_h, d_v } t_dir;

typedef struct { int x, y, sx, sy; t_dir dir; } t_Coup, *p_Coup;
static t_Coup coups_possibles [Ncoups];
static size_t taille_coups_possibles = Ncoups;
static int nb_coups_possibles;

typedef struct S_Cellule {
  int x, y;
  int numero;
  int counts [3][3];
  int connected [3][3];
} t_Cellule, *p_Cellule;

#define N 50
typedef t_Cellule t_Jeu [N][N];
typedef struct {
  t_Jeu jeu;
  int min_x, min_y, max_x, max_y;
} t_Game;
static t_Game game;

static int nb_croix_init = 36;
static int x_init[] = { 20, 20, 20, 20, 21, 22, 23, 23, 23, 23,
		       24, 25, 26, 26, 26, 26, 27, 28, 29,
		       29, 29, 29, 28, 27, 26, 26, 26, 26,
		       25, 24, 23, 23, 23, 23, 22, 21 };
static int y_init[] = { 23, 24, 25, 26, 26, 26, 26, 27, 28, 29,
		       29, 29, 29, 28, 27, 26, 26, 26, 26,
		       25, 24, 23, 23, 23, 23, 22, 21, 20,
		       20, 20, 20, 21, 22, 23, 23, 23 };

#define NB_COUPS_MAX 300
static t_Coup tous_les_coups [NB_COUPS_MAX];
static int nb_coups = 0;

/* (i, j) : coordonnées case
   ii et jj \in { -1, 0, +1 } : voisin considéré */
static int compte (const int idx, const int i, const int j,
		   const int ii, const int jj)
{
  int d;
  for (d = 1; (game. jeu [i + d * ii] [j + d * jj]. counts [1][1] == 1)
	 && (! game. jeu [i + d * ii] [j + d * jj]. connected [1 - ii] [1 - jj])
	 ; d++);
  return (d - 1);
}

static void init_jeu (int idx)
{
  game. min_x = game. min_y = 20;
  game. max_x = game. max_y = 29;
  for (int i = 0; i < N; i ++)
    for (int j = 0; j < N; j ++) {
      game. jeu [i] [j]. x = i;
      game. jeu [i] [j]. y = j;
      game. jeu [i] [j]. numero = 0;
      for (int ii = 0; ii < 3; ii ++)
	for (int jj = 0; jj < 3; jj ++) {
	  game. jeu [i] [j]. counts [ii] [jj] = 0;
	  game. jeu [i] [j]. connected [ii] [jj] = 0;
	}
    }
  for (int i = 0; i < nb_croix_init; i ++)
    game. jeu [x_init [i]] [y_init [i]]. counts [1] [1] = 1;

  for (int i = 19; i < 31; i ++)
    for (int j = 19; j < 31; j ++)
      for (int ii = 0; ii < 3; ii ++)
	for (int jj = 0; jj < 3; jj ++)
	  if ((ii != 1) || (jj != 1)) {
	    int c;
	    c = compte (idx, i, j, ii - 1, jj - 1);
	    game. jeu [i] [j]. counts [ii] [jj] = c;
	  }
}

static void print_jeu (const int idx)
{
  int xmin = game. min_x, xmax = game. max_x,
    ymin = game. min_y, ymax = game. max_y;

  for (int y = ymax; y >= ymin; y --) {
    for (int x = xmin; x <= xmax; x ++) {
      if (game. jeu [x] [y]. counts [1] [1] == 1)
	if ((game. jeu [x] [y]. connected [0] [1]) ||
	    (game. jeu [x] [y]. connected [2] [1]))
	  putchar ('-');
	else if ((game. jeu [x] [y]. connected [1] [0]) ||
		 (game. jeu [x] [y]. connected [1] [2]))
	  putchar ('|');
	else if ((game. jeu [x] [y]. connected [0] [0]) ||
		 (game. jeu [x] [y]. connected [2] [2]))
	  putchar ('/');
	else if ((game. jeu [x] [y]. connected [0] [2]) ||
		 (game. jeu [x] [y]. connected [2] [0]))
	  putchar ('\\');
	else putchar ('.');
      else putchar (' ');
    }
    putchar ('\n');
  }
}

/*
 * Returns all the possible play in array coups_possible [idx]
 */
static void ou_peut_on_jouer (const int idx, const int level)
{
  int count = 0;
  for (int x = game. min_x - 1; x <= game. max_x + 1; x ++)
    for (int y = game. min_y - 1; y <= game. max_y + 1; y ++)
      if (game. jeu [x] [y]. counts [1] [1] == 0) {
	int l = game. jeu [x] [y]. counts [0] [0] +
	  game. jeu [x] [y]. counts [2] [2];
	if (l == 4) {
	  coups_possibles [count]. x = x;
	  coups_possibles [count]. y = y;
	  coups_possibles [count]. sx =
	    x - game. jeu [x] [y]. counts [0] [0];
	  coups_possibles [count]. sy =
	    y - game. jeu [x] [y]. counts [0] [0];
	  coups_possibles [count]. dir = d_a;
	  count ++;
	} else if (l > 4) {
	  int sx = x - game. jeu [x] [y]. counts [0] [0];
	  int sy = y - game. jeu [x] [y]. counts [0] [0];
	  for (int i = 0; i <= l - 4; i++)
	    if ((x <= sx + i + 4)  && (x >= sx + i)) {
	      coups_possibles [count]. x = x;
	      coups_possibles [count]. y = y;
	      coups_possibles [count]. sx = sx + i;
	      coups_possibles [count]. sy = sy + i;
	      coups_possibles [count]. dir = d_a;
	      count ++;
	    }
	}
	l = game. jeu [x] [y]. counts [0] [2] +
	  game. jeu [x] [y]. counts [2] [0];
	if (l == 4) {
	  coups_possibles [count]. x = x;
	  coups_possibles [count]. y = y;
	  coups_possibles [count]. sx = x -
	    game. jeu [x] [y]. counts [0] [2];
	  coups_possibles [count]. sy = y +
	    game. jeu [x] [y]. counts [0] [2];
	  coups_possibles [count]. dir = d_d;
	  count ++;
	} else if (l > 4) {
	  int sx = x - game. jeu [x] [y]. counts [0] [2];
	  int sy = y + game. jeu [x] [y]. counts [0] [2];
	  for (int i = 0; i <= l - 4; i++)
	    if ((x <= sx + i + 4)  && (x >= sx + i)) {
	      coups_possibles [count]. x = x;
	      coups_possibles [count]. y = y;
	      coups_possibles [count]. sx = sx + i;
	      coups_possibles [count]. sy = sy - i;
	      coups_possibles [count]. dir = d_d;
	      count ++;
	    }
	}
	l = game. jeu [x] [y]. counts [0] [1] +
	  game. jeu [x] [y]. counts [2] [1];
	if (l == 4) {
	  coups_possibles [count]. x = x;
	  coups_possibles [count]. y = y;
	  coups_possibles [count]. sx = x -
	    game. jeu [x] [y]. counts [0] [1];
	  coups_possibles [count]. sy = y;
	  coups_possibles [count]. dir = d_h;
	  count ++;
	} else if (l > 4) {
	  int sx = x - game. jeu [x] [y]. counts [0] [1];
	  for (int i = 0; i <= l - 4; i++)
	    if ((x <= sx + i + 4)  && (x >= sx + i)) {
	      coups_possibles [count]. x = x;
	      coups_possibles [count]. y = y;
	      coups_possibles [count]. sx = sx + i;
	      coups_possibles [count]. sy = y;
	      coups_possibles [count]. dir = d_h;
	      count ++;
	    }
	}
	l = game. jeu [x] [y]. counts [1] [0] +
	  game. jeu [x] [y]. counts [1] [2];
	if (l == 4) {
	  coups_possibles [count]. x = x;
	  coups_possibles [count]. y = y;
	  coups_possibles [count]. sx = x;
	  coups_possibles [count]. sy = y -
	    game. jeu [x] [y]. counts [1] [0];
	  coups_possibles [count]. dir = d_v;
	  count ++;
	} else if (l > 4) {
	  int sy = y - game. jeu [x] [y]. counts [1] [0];
	  for (int i = 0; i <= l - 4; i++)
	    if ((y <= sy + i + 4) && (y >= sy + i)) {
	      coups_possibles [count]. x = x;
	      coups_possibles [count]. y = y;
	      coups_possibles [count]. sx = x;
	      coups_possibles [count]. sy = sy + i;
	      coups_possibles [count]. dir = d_v;
	      count ++;
	    }
	}
      }
  nb_coups_possibles = count;
}

static void mise_a_jour_horizontale_2 (int idx, const int sx, const int sy)
{
  for (int i = 1; i <= game. jeu [sx] [sy]. counts [0] [1] + 1; i ++)
    game. jeu [sx - i] [sy]. counts [2] [1] = i;
  for (int i = 1; i <= game. jeu [sx + 4] [sy]. counts [2] [1] + 1; i ++)
    game. jeu [sx + 4 + i] [sy]. counts [0] [1] = i;
}
static void mise_a_jour_horizontale (int idx, const int x, const int y)
{
  for (int i = 1; i <= game. jeu [x] [y]. counts [0] [1] + 1; i ++)
    for (int ii = 0; ii < 3; ii ++)
      for (int jj = 0; jj < 3; jj ++)
	if ((ii != 1) || (jj != 1))
	  game. jeu [x - i] [y]. counts [ii] [jj] =
	    compte (idx, x - i, y, ii - 1, jj - 1);
  for (int i = 1; i <= game. jeu [x] [y]. counts [2] [1] + 1; i ++)
    for (int ii = 0; ii < 3; ii ++)
      for (int jj = 0; jj < 3; jj ++)
	if ((ii != 1) || (jj != 1))
	  game. jeu [x + i] [y]. counts [ii] [jj] =
	    compte (idx, x + i, y, ii - 1, jj - 1);
}

static void mise_a_jour_verticale_2 (int idx, const int sx, const int sy)
{
  for (int i = 1; i <= game. jeu [sx] [sy]. counts [1] [0] + 1; i ++)
    game. jeu [sx] [sy - i]. counts [1] [2] = i;
  for (int i = 1; i <= game. jeu [sx] [sy + 4]. counts [1] [2] + 1; i ++)
    game. jeu [sx] [sy + 4 + i]. counts [1] [0] = i;
}

static void mise_a_jour_verticale (int idx, const int x, const int y)
{
  for (int i = 1; i <= game. jeu [x] [y]. counts [1] [0] + 1; i ++)
    for (int ii = 0; ii < 3; ii ++)
      for (int jj = 0; jj < 3; jj ++)
	if ((ii != 1) || (jj != 1))
	  game. jeu [x] [y - i]. counts [ii] [jj] =
	    compte (idx, x, y - i, ii - 1, jj - 1);
  for (int i = 1; i <= game. jeu [x] [y]. counts [1] [2] + 1; i ++)
    for (int ii = 0; ii < 3; ii ++)
      for (int jj = 0; jj < 3; jj ++)
	if ((ii != 1) || (jj != 1))
	  game. jeu [x] [y + i]. counts [ii] [jj] =
	    compte (idx, x, y + i, ii - 1, jj - 1);
}

static void mise_a_jour_adiagonale_2 (int idx, const int sx, const int sy)
{
  for (int i = 1; i <= game. jeu [sx] [sy]. counts [0] [0] + 1; i ++)
    game. jeu [sx - i] [sy - i]. counts [2] [2] = i;
  for (int i = 1; i <= game. jeu [sx + 4] [sy + 4]. counts [2] [2] + 1; i ++)
    game. jeu [sx + 4 + i] [sy + 4 + i]. counts [0] [0] = i;
}

static void mise_a_jour_adiagonale (int idx, const int x, const int y)
{
  for (int i = 1; i <= game. jeu [x] [y]. counts [0] [0] + 1; i ++)
    for (int ii = 0; ii < 3; ii ++)
      for (int jj = 0; jj < 3; jj ++)
	if ((ii != 1) || (jj != 1))
	  game. jeu [x - i] [y - i]. counts [ii] [jj] =
	    compte (idx, x - i, y - i, ii - 1, jj - 1);
  for (int i = 1; i <= game. jeu [x] [y]. counts [2] [2] + 1; i ++)
    for (int ii = 0; ii < 3; ii ++)
      for (int jj = 0; jj < 3; jj ++)
	if ((ii != 1) || (jj != 1))
	  game. jeu [x + i] [y + i]. counts [ii] [jj] =
	    compte (idx, x + i, y + i, ii - 1, jj - 1);
}

static void mise_a_jour_diagonale_2 (int idx, const int sx, const int sy)
{
  for (int i = 1; i <= game. jeu [sx] [sy]. counts [0] [2] + 1; i ++)
    game. jeu [sx - i] [sy + i]. counts [2] [0] = i;
  for (int i = 1; i <= game. jeu [sx + 4] [sy - 4]. counts [2] [0] + 1; i ++)
    game. jeu [sx + 4 + i] [sy - 4 - i]. counts [0] [2] = i;
}

static void mise_a_jour_diagonale (int idx, const int x, const int y)
{
  for (int i = 1; i <= game. jeu [x] [y]. counts [0] [2] + 1; i ++)
    for (int ii = 0; ii < 3; ii ++)
      for (int jj = 0; jj < 3; jj ++)
	if ((ii != 1) || (jj != 1))
	  game. jeu [x - i] [y + i]. counts [ii] [jj] =
	    compte (idx, x - i, y + i, ii - 1, jj - 1);
  for (int i = 1; i <= game. jeu [x] [y]. counts [2] [0] + 1; i ++)
    for (int ii = 0; ii < 3; ii ++)
      for (int jj = 0; jj < 3; jj ++)
	if ((ii != 1) || (jj != 1))
	  game. jeu [x + i] [y - i]. counts [ii] [jj] =
	    compte (idx, x + i, y - i, ii - 1, jj - 1);
}

static int jouer_a (int idx, const int x, const int y,
		    const int sx, const int sy)
{
  if (x - sx != y - sy) return 0;
  int l = game. jeu [x] [y]. counts [0] [0] +
    game. jeu [x] [y]. counts [2] [2]; /* -diag */
  if (l < 4) {
    printf ("pas 5 croix alignées (%d, %d, %d, %d, a)\n", x, y, sx, sy);
    return 0;
  }

  /* on marque les 5 croix comme connectées */
  for (int i = 0; i < 5; i ++) {
    if (i != 4) {
      game. jeu [sx + i] [sy + i]. connected [2] [2] = 1;
      game. jeu [sx + i] [sy + i]. counts [2] [2] = 0;
    }
    if (i) {
      game. jeu [sx + i] [sy + i]. connected [0] [0] = 1;
      game. jeu [sx + i] [sy + i]. counts [0] [0] = 0;
    }
  }
  
  mise_a_jour_verticale (idx, x, y);
  mise_a_jour_horizontale (idx, x, y);
  mise_a_jour_diagonale (idx, x, y);
  mise_a_jour_adiagonale_2 (idx, sx, sy);

  return 1;
}

static int jouer_d (int idx, const int x, const int y,
		    const int sx, const int sy)
{
  if (x - sx != sy - y) return 0;
  int l = game. jeu [x] [y]. counts [0] [2] + game. jeu [x] [y]. counts [2] [0]; /* diag */
  if (l < 4) {
    printf ("pas 5 croix alignées (%d, %d, %d, %d, d)\n", x, y, sx, sy);
    return 0;
  }

  /* on marque les 5 croix comme connectées verticalement */
  for (int i = 0; i < 5; i ++) {
    if (i != 4) {
      game. jeu [sx + i] [sy - i]. connected [2] [0] = 1;
      game. jeu [sx + i] [sy - i]. counts [2] [0] = 0;
    }
    if (i) {
      game. jeu [sx + i] [sy - i]. connected [0] [2] = 1;
      game. jeu [sx + i] [sy - i]. counts [0] [2] = 0;
    }
  }
  
  mise_a_jour_verticale (idx, x, y);
  mise_a_jour_horizontale (idx, x, y);
  mise_a_jour_adiagonale (idx, x, y);
  mise_a_jour_diagonale_2 (idx, sx, sy);

  return 1;
}

static int jouer_v (int idx, const int x, const int y,
		    const int sx, const int sy)
{
  if ((x != sx) || (y < sy)) return 0;
  int l = game. jeu [x] [y]. counts [1] [0] +
    game. jeu [x] [y]. counts [1] [2]; /* - */
  if (l < 4) {
    printf ("pas 5 croix alignées (%d, %d, %d, %d, v)\n", x, y, sx, sy);
    return 0;
  }
  if (sy < y - game. jeu [x] [y]. counts [1] [0]) return 0;
  if (sy > y + game. jeu [x] [y]. counts [1] [2]) return 0;

  /* on marque les 5 croix comme connectées verticalement */
  for (int i = 0; i < 5; i ++) {
    if (i != 4) {
      game. jeu [x] [sy + i]. connected [1] [2] = 1;
      game. jeu [x] [sy + i]. counts [1] [2] = 0;
    }
    if (i) {
      game. jeu [x] [sy + i]. connected [1] [0] = 1;
      game. jeu [x] [sy + i]. counts [1] [0] = 0;
    }
  }
  
  mise_a_jour_horizontale (idx, x, y);
  mise_a_jour_verticale_2 (idx, sx, sy);
  mise_a_jour_diagonale (idx, x, y);
  mise_a_jour_adiagonale (idx, x, y);
  return 1;
}

static int jouer_h (int idx, const int x, const int y,
		    const int sx, const int sy)
{
  if ((x < sx) || (y != sy)) return 0;
  /* on vérifie que l'on a bien fait une lignes horizontales de 5 croix */
  int l = game. jeu [x] [y]. counts [0] [1] + game. jeu [x] [y]. counts [2] [1]; /* -- */
  if (l < 4) {
    printf ("pas 5 croix alignées (%d, %d, %d, %d, h)\n", x, y, sx, sy);
    return 0;
  }
  if (sx < x - game. jeu [x] [y]. counts [0] [1]) return 0;
  if (sx > x + game. jeu [x] [y]. counts [2] [1]) return 0;

  /* on marque les 5 croix comme connectées horizontalement */
  for (int i = 0; i < 5; i ++) {
    if (i != 4) {
      game. jeu [sx + i] [y]. connected [2] [1] = 1;
      game. jeu [sx + i] [y]. counts [2] [1] = 0;
    }
    if (i) {
      game. jeu [sx + i] [y]. connected [0] [1] = 1;
      game. jeu [sx + i] [y]. counts [0] [1] = 0;
    }
  }

  mise_a_jour_horizontale_2 (idx, sx, sy);
  mise_a_jour_verticale (idx, x, y);
  mise_a_jour_diagonale (idx, x, y);
  mise_a_jour_adiagonale (idx, x, y);
  
  return 1;
}

/* Returns 0 if not possible to play (x, y), 1 if ok. */
/* Should play a cross in (x, y), the line starting at (sx, sy),
   going either rightward (increasing x) from sx, 
   or upward (increasing y) from sy. */
static int jouer_un_coup (int idx, const int x, const int y,
			  const int sx, const int sy,
			  const t_dir direction, const int coup)
{
  if (game. jeu [x] [y]. counts [1] [1]) {
    printf ("case déjà occupée (%d, %d)\n", x, y);
    return 0;
  }
  game. jeu [x] [y]. counts [1] [1] = 1;
  game. jeu [x] [y]. numero = coup;

  if (x < game. min_x) game. min_x = x;
  if (x > game. max_x) game. max_x = x;
  if (y < game. min_y) game. min_y = y;
  if (y > game. max_y) game. max_y = y;

  switch (direction) {
  case d_d: return jouer_d (idx, x, y, sx, sy);
  case d_a: return jouer_a (idx, x, y, sx, sy);
  case d_h: return jouer_h (idx, x, y, sx, sy);
  case d_v: return jouer_v (idx, x, y, sx, sy);
  }
  return 1;
}

static int jouer (int idx, const int niveau, const int which,
		  const int coup)
{
  /* assert (which < nb_coups_possibles); */
  return jouer_un_coup (idx, coups_possibles [which]. x,
			coups_possibles [which]. y,
			coups_possibles [which]. sx,
			coups_possibles [which]. sy,
			coups_possibles [which]. dir, coup);
}

/* ****************************** */
/* extern functions use as facade */

extern void facade_init(void)
{
}

extern void facade_reset_game(void)
{
  init_jeu(0);
}

extern void facade_step(int which_move, int move_no)
{
  jouer (0, 0, which_move, move_no);
}

extern void facade_print_game(void)
{
  print_jeu(0);
}

extern void facade_update_available_move(void)
{
  ou_peut_on_jouer(0,0);
}

extern int facade_get_nb_available_moves(void)
{
  return nb_coups_possibles;
}

extern int facade_get_board_game_size(void)
{
  return N;
}

extern void facade_get_available_moves(t_Coup available_moves[])
{  
  for (int i = 0; i < nb_coups_possibles; i++)
    available_moves[i] = coups_possibles[i];
}

extern void facade_get_board_game_information(t_Jeu board_game_information)
{  
  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
      board_game_information[i][j] = game.jeu[i][j];
}

/* End of extern functions use as facade */

